public class program {

  Day[] days;
  student  student;

  public program(Day[] days, student student) {
    this.days = days;
    this.student = student;
  }

  public Day[] getDays() {
    return days;
  }

  public void setDays(Day[] days) {
    this.days = days;
  }

  public student getStudent() {
    return student;
  }

  public void setStudent(student student) {
    this.student = student;
  }
}

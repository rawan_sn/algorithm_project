import java.util.ArrayList;
import java.util.List;

public class student {

   private  int studentId;
   private String name;
   private int numberOfSubjects;
   private List<subject> subjectList;

   // إضافة الرغبات للأيام والأوقات


    public student(int studentId, String name, int numberOfSubjects, List<subject> subjectList) {
        this.studentId = studentId;
        this.name = name;
        this.numberOfSubjects = numberOfSubjects;
        subjectList=new ArrayList<>();
        this.subjectList=subjectList;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfSubjects() {
        return numberOfSubjects;
    }

    public void setNumberOfSubjects(int numberOfSubjects) {
        this.numberOfSubjects = numberOfSubjects;
    }
}

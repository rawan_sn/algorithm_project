public class subject {

    String subjectName;
    subjectType type;
    String subjectId;

    public subject(String subjectName, subjectType type, String subjectId) {
        this.subjectName = subjectName;
        this.type = type;
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public subjectType getType() {
        return type;
    }

    public void setType(subjectType type) {
        this.type = type;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }
}
